import React, { useState, useEffect } from "react";
import Board from "./Board";
import Swal from 'sweetalert2'

const Game = () => {

    //State for recording the position
    const [history, setHistory] = useState([Array(9).fill(null)]);
    //State for square number
    const [stepNumber, setStepNumber] = useState(0);
    //to alternate the mark
    const [xIsNext, setXisNext] = useState(true);
    const [winner, setWinner] = useState(null)
    //Mark checker
    const xO = xIsNext ? "X" : "O";

    const handleClick = async (i) => {

    const current = history[stepNumber];
    const squares = [...current];
    
    console.log(history[stepNumber])
    console.log(stepNumber)
    // return if won or occupied
    if (squares[i] || winner){
        return
    }

    // select square
    squares[i] = xO;
    //Checking the record position
    setHistory([...history, squares]);
    setStepNumber(history.length);
    setXisNext(!xIsNext);
    };

    const refresh = () => {
        setStepNumber(0)
    }

    useEffect(() => {
        fetch('https://pacific-lake-14033.herokuapp.com/api', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            evaluateGame : history[stepNumber]
        })
        })
        .then(res => {
            return res.text()
        })
        .then(data => {
            setWinner(data)
        })
    }, [stepNumber, history])

    useEffect(() => {
        if(winner === "X" || winner === "O"){
          Swal.fire({
            title: `PLAYER ${winner} WON!`,
            icon: 'icon',
          })
        }
        if(winner === "draw"){
          Swal.fire({
            title: `DRAW`,
            icon: 'incon',
            
          })
        }
    }, [winner])

  return (
    <>
      <div className="info-wrapper">
        <h3>{winner ? winner === "draw" ? "DRAW" : "WINNER: " + winner  : "NEXT PLAYER: " + xO}</h3>
        <button onClick={refresh}>REFRESH</button>
      </div>
      <Board squares={history[stepNumber]} onClick={handleClick} />
    </>
  );
};

export default Game;