This tic-tac-toe project was developed using ReactJs and NodeJs with ExpressJs framework. 

For frontend development using React, I created three components (Board, Game, Square). The board component is the parent of Square component which is responsible for mapping the tiles in the game. Moreover, it tracks down the value of each squares to perform the logic of evaluating the state of the game. 

The Square is the clickable field in the game which displays the value of the mark ("X", "O"). 

The last component, Game, is the topmost component which handles the state of the whole game. I created the history state to record the occupied tiles in the array. Then, the data will be passed to the backend by calling the API and the server will call the logic to evaluate the game if there's already a winner or if it's a draw.

In addition, I implemented a logic in the handle click function to make the squares unclickable if there's already a declared winner or if the tile is already occupied. I've also created a button to restart the game.

For the backend, I used ExpressJs to easily configure the server. There's only one route and function to check if the game is over. I hardcoded the winning combinations in the logic function and iterate it using a for loop that will check if the data passed matches in one of the winning combination. I also included the logic to check if all tiles are occupied which will send draw to the frontend. 

On a side note, I deployed this project on netlify for the frontend: https://tictactoe-hive-vindniega.netlify.app/ and heroku for the backend. 